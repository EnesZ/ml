import numpy as np
from sklearn.datasets.samples_generator import make_blobs
import matplotlib.pyplot as plt

x,y=make_blobs(n_samples=160, centers=2, random_state=0,cluster_std=0.50)
plt.scatter(x[:,0],x[:,1],c=y,cmap='autumn')

# "Support vector classifier"
from sklearn.svm import SVC
model = SVC(kernel='linear', C=0.1)
model.fit(x, y)

#get current axes
ax=plt.gca()
xlim = ax.get_xlim()
ylim = ax.get_ylim()

# create grid to evaluate model
x = np.linspace(xlim[0], xlim[1], 30)
y = np.linspace(ylim[0], ylim[1], 30)
Y, X = np.meshgrid(y, x)
xy = np.vstack([X.ravel(), Y.ravel()]).T
P = model.decision_function(xy).reshape(X.shape)

# plot decision boundary and margins
ax.contour(X, Y, P, colors='k',
               levels=[-1, 0, 1], alpha=0.5,
               linestyles=['--', '-', '--'])

ax.set_xlim(xlim)
ax.set_ylim(ylim)

plt.scatter(model.support_vectors_[:, 0],model.support_vectors_[:, 1],s=300, linewidth=1, facecolors='none');
plt.show()