import numpy as np
import matplotlib.pyplot as plt

points=np.concatenate((np.random.rand(50,2)*5,np.random.rand(50,2)*5+2),axis=0)
x=np.random.rand(2,2)*5
plt.scatter(points[:, 0], points[:, 1])
plt.show()

plt.scatter(x[0,0],x[0,1],color='b',linewidth='5')
plt.scatter(x[1,0],x[1,1],color='r',linewidth='5')
plt.scatter(points[:, 0], points[:, 1])
plt.show()

def distance(x1,x2):
    x=np.power(x1[0]-x2[0],2)
    y=np.power(x1[1]-x2[1],2)
    return np.sqrt(x+y)

for _ in range(10):
    c1 = []
    c2 = []
    for y in points:
        if distance(y,x[0])<distance(y,x[1]):
            c1.append(y.tolist())
        else:
            c2.append(y.tolist())

    c1_array=np.asanyarray(c1)
    c2_array=np.asanyarray(c2)

    plt.scatter(c1_array[:, 0], c1_array[:, 1], color='b')
    plt.scatter(c2_array[:, 0], c2_array[:, 1], color='r')
    plt.scatter(x[0, 0], x[0, 1], color='b', linewidth='5')
    plt.scatter(x[1, 0], x[1, 1], color='r', linewidth='5')
    plt.show()
    x[0] = np.array([np.mean(c1_array[:, 0]), np.mean(c1_array[:, 1])])
    x[1] = np.array([np.mean(c2_array[:, 0]), np.mean(c2_array[:, 1])])