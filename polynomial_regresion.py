import numpy as np
import matplotlib.pyplot as plt

x_train = np.array([10,15,20,25,30,35,40,45,50,55,10,15,20,25,30,35,40,45,50,55])
y_train= np.array([10,20,25,30,31,32,32,33,33,34,11,21,28,31,34,34,35,35,36,36])

def model(x,w1,w2,b):
    return x*w1+np.sqrt(x)*w2+b;

def cost(y,y_hat):
    return np.sum((y-y_hat)**2)/y.size

learning_rate=0.0007
def trainning_round(x_train,y_train,w1,w2,b,learning_rate):

    y_hat=model(x_train,w1,w2,b)
    print(cost(y_train,y_hat))

    w1_gradient=-2*x_train.dot(y_train-y_hat)/y_train.size
    w2_gradient = -2 * np.sqrt(x_train).dot(y_train - y_hat) / y_train.size
    b_gradient=-2*np.sum(y_train-y_hat)/y_train.size

    w1-=learning_rate*w1_gradient
    w2-=learning_rate*w2_gradient
    b-=learning_rate*b_gradient

    return w1,w2,b

num_epoch=450000
def train(X,Y):

    w1=0
    w2=0
    b=-5

    #for plt
    ar = np.arange(0, 60, 0.5)

    for i in range(num_epoch):
            w1,w2,b=trainning_round(X,Y,w1,w2,b,learning_rate)
            if i%15000==0 and i>1:
                plt.plot(ar, model(ar, w1, w2, b))
            if i == num_epoch-1:
                plt.plot(ar, model(ar, w1, w2, b),linewidth=4, color='r')
    plt.axis([0, 60, 0, 50])
    plt.plot(X, Y, 'ro')

train(x_train,y_train)
plt.show()

