(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     27651,        883]
NotebookOptionsPosition[     25143,        798]
NotebookOutlinePosition[     25521,        814]
CellTagsIndexPosition[     25478,        811]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{"x_train", "=", 
    RowBox[{
     RowBox[{
      RowBox[{"np", ".", "array"}], 
      RowBox[{"(", 
       RowBox[{"[", 
        RowBox[{"170", ",", "175", ",", "188", ",", "192"}], "]"}], ")"}], 
      "\n", "y_train"}], "=", 
     RowBox[{
      RowBox[{"np", ".", "array"}], 
      RowBox[{"(", 
       RowBox[{"[", 
        RowBox[{"75", ",", "79", ",", "86", ",", "88"}], "]"}], ")"}]}]}]}], 
   "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"set", "=", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"170", ",", "75"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"175", ",", "79"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"188", ",", "86"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"192", ",", "88"}], "}"}]}], "}"}]}], ";"}]}]], "Input",
 CellChangeTimes->{{3.7228633241399555`*^9, 3.722863339582839*^9}, {
  3.7228634412516537`*^9, 3.7228634833690634`*^9}, {3.7409035116920705`*^9, 
  3.740903553316451*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"m", "=", 
  RowBox[{"ListPlot", "[", "set", "]"}]}]], "Input",
 CellChangeTimes->{{3.7228634871472793`*^9, 3.7228634927896023`*^9}, {
  3.7228643251772118`*^9, 3.7228643261482673`*^9}}],

Cell[BoxData[
 GraphicsBox[{{}, {{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.012833333333333334`],
      AbsoluteThickness[1.6], 
     PointBox[{{170., 75.}, {175., 79.}, {188., 86.}, {192., 
      88.}}]}, {}}, {}, {}, {{}, {}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{169.65625, 74.35},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  Method->{"CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Part[{{Identity, Identity}, {Identity, Identity}}, 1, 2][#]& )[
         Part[#, 1]], 
        (Part[{{Identity, Identity}, {Identity, Identity}}, 2, 2][#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Part[{{Identity, Identity}, {Identity, Identity}}, 1, 2][#]& )[
         Part[#, 1]], 
        (Part[{{Identity, Identity}, {Identity, Identity}}, 2, 2][#]& )[
         Part[#, 2]]}& )}},
  PlotRange->{{170., 192.}, {75., 88.}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{3.722863494937725*^9, 3.7228643270433187`*^9, 
  3.74090356169293*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"r", "[", 
    RowBox[{"b0_", ",", "b1_"}], "]"}], ":=", 
   RowBox[{
    FractionBox["1", 
     RowBox[{"2", "*", 
      RowBox[{"Length", "[", "set", "]"}]}]], "*", 
    RowBox[{"Sum", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{"set", "[", 
          RowBox[{"[", 
           RowBox[{"i", ",", "2"}], "]"}], "]"}], "-", "b0", "-", 
         RowBox[{"b1", "*", 
          RowBox[{"set", "[", 
           RowBox[{"[", 
            RowBox[{"i", ",", "1"}], "]"}], "]"}]}]}], ")"}], "^", "2"}], ",", 
      RowBox[{"{", 
       RowBox[{"i", ",", "1", ",", 
        RowBox[{"Length", "[", "set", "]"}]}], "}"}]}], "]"}]}]}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.722863526694541*^9, 3.722863583112768*^9}, {
  3.7228636136225133`*^9, 3.722863647116429*^9}, {3.7228636816894064`*^9, 
  3.72286368786576*^9}, {3.722863742120863*^9, 3.722863742720897*^9}, {
  3.722863788120494*^9, 3.722863828657812*^9}, {3.722863933445806*^9, 
  3.722863985405778*^9}, {3.7228640156305065`*^9, 3.7228640709936733`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"D", "[", 
  RowBox[{
   RowBox[{"r", "[", 
    RowBox[{"b0", ",", "b1"}], "]"}], ",", "b1"}], "]"}]], "Input",
 CellChangeTimes->{{3.722864080746231*^9, 3.7228641312321186`*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "8"], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{
     RowBox[{"-", "384"}], " ", 
     RowBox[{"(", 
      RowBox[{"88", "-", "b0", "-", 
       RowBox[{"192", " ", "b1"}]}], ")"}]}], "-", 
    RowBox[{"376", " ", 
     RowBox[{"(", 
      RowBox[{"86", "-", "b0", "-", 
       RowBox[{"188", " ", "b1"}]}], ")"}]}], "-", 
    RowBox[{"350", " ", 
     RowBox[{"(", 
      RowBox[{"79", "-", "b0", "-", 
       RowBox[{"175", " ", "b1"}]}], ")"}]}], "-", 
    RowBox[{"340", " ", 
     RowBox[{"(", 
      RowBox[{"75", "-", "b0", "-", 
       RowBox[{"170", " ", "b1"}]}], ")"}]}]}], ")"}]}]], "Output",
 CellChangeTimes->{{3.7228640999903316`*^9, 3.722864137142457*^9}, 
   3.740903572892571*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"s", "=", 
  RowBox[{
   RowBox[{
    RowBox[{"{", 
     RowBox[{"b0", ",", "b1"}], "}"}], "/.", 
    RowBox[{"Solve", "[", 
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{"D", "[", 
         RowBox[{
          RowBox[{"r", "[", 
           RowBox[{"b0", ",", "b1"}], "]"}], ",", "b1"}], "]"}], "==", "0"}], 
       "&&", 
       RowBox[{
        RowBox[{"D", "[", 
         RowBox[{
          RowBox[{"r", "[", 
           RowBox[{"b0", ",", "b1"}], "]"}], ",", "b0"}], "]"}], "==", 
        "0"}]}], ",", 
      RowBox[{"{", 
       RowBox[{"b0", ",", "b1"}], "}"}]}], "]"}]}], "//", "N"}]}]], "Input",
 CellChangeTimes->{{3.7228641586086845`*^9, 3.722864176631716*^9}, {
  3.7228642067024355`*^9, 3.722864228092659*^9}, {3.740903585513293*^9, 
  3.7409035861023264`*^9}, {3.740903631692934*^9, 3.740903693812487*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"{", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"-", "22.839326702371842`"}], ",", "0.5784238714613619`"}], 
    "}"}], "}"}], "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.7409037158677487`*^9, 3.740903751410782*^9}}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.7409037553040047`*^9, 3.7409037553430066`*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"f", "[", "x_", "]"}], ":=", 
   RowBox[{
    RowBox[{
     RowBox[{"s", "[", 
      RowBox[{"[", "1", "]"}], "]"}], "[", 
     RowBox[{"[", "1", "]"}], "]"}], "+", 
    RowBox[{"x", "*", 
     RowBox[{
      RowBox[{"s", "[", 
       RowBox[{"[", "1", "]"}], "]"}], "[", 
      RowBox[{"[", "2", "]"}], "]"}]}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"m1", "=", 
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"f", "[", "x", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"x", ",", "0", ",", "200"}], "}"}]}], "]"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.7228642452006373`*^9, 3.7228642462156954`*^9}, {
  3.722864280879678*^9, 3.7228643137235565`*^9}, {3.7228644121231847`*^9, 
  3.7228644337594223`*^9}, {3.740903604990407*^9, 3.7409036061994762`*^9}, {
  3.7409036983047442`*^9, 3.7409037090433583`*^9}, {3.7409037595682483`*^9, 
  3.740903777279261*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Show", "[", 
  RowBox[{"m", ",", "m1"}], "]"}]], "Input",
 CellChangeTimes->{{3.7228643308775377`*^9, 3.72286435678802*^9}, {
  3.722864387324766*^9, 3.722864401340568*^9}, {3.7228644381776752`*^9, 
  3.722864439570755*^9}}],

Cell[BoxData[
 GraphicsBox[{{{}, {{}, 
     {RGBColor[0.368417, 0.506779, 0.709798], PointSize[
      0.012833333333333334`], AbsoluteThickness[1.6], 
      PointBox[{{170., 75.}, {175., 79.}, {188., 86.}, {192., 
       88.}}]}, {}}, {}, {}, {{}, {}}}, {{{}, {}, 
     TagBox[
      {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], 
       Opacity[1.], LineBox[CompressedData["
1:eJwVkXk41HkAh5lqaDfrSEnmCZNVUmExpDSfJlcYjWsGNXI05uvKveXISq1F
ckRWKzYrsepBFB3kxyNqSgexu9jKVoanY0V2oh3W/vE+7/P+/eoHRXmIaAoK
CtxF/jczT15Zvu7JzhLXOzN/DrKoFmUj3UhSz3ZXZ9+828uiOo7UXdhI2tnL
B6+nNnWxqG7TGDGdPGInHbislNXIou7m9DnNiJ+zhbGF2ha5LOqezfEbb8V/
s/VLAm2z7VmUpGA4Zly8wK59KT/BumpJObcpuY2JVdGaxNLMK7CgelVYy5cK
9VC7/d0VP51vKE6ePVW9dyNyh0fU+8tMKJHJnIqZiimsX0Y30xnGFNdI9GG1
owUOq+X7RlkbUo1Pjybcs7bGSj27KP8afWq7xqb6pd/tQGYtXWlZpDYl/ny6
rMoHyG/hbZs6pEpFdUvN67kcqDk+Eo5XzrVn+9G/lkfYgSKe3//mNMEO7kkO
MSMOOPLQdoNDjTI6lLTdw32dkLI2es8CbxXM1NOKJ+KdUUQzTo9R10XAtO1m
twhXfNJ4Fha+ygAV5XcYslNu0PxOZ/XEEiM05doM93nw0OrYI+ENb0H8TVte
mas7aFpR9LMmZmiXVE1l7fOAv8xiY9Qv5gh13NYW6uOJnJAGtfQ3ljhXY3Yt
PMwL7zljiUkiaxRMtx2LPuyNYm6/VNpvg7HahG9JLB8a0gTG6TW26HI4XZKe
LkDXu74n+rlsKO4d+isvzQeVHVOGsreAwq5mxfOJvsisFEi1VDjI1JJx+37w
g3KrNrlkvxvRfsGdsuP7MFnKchWG2mH0mMVB9pn9SGhsmDxWZQ/NwrNDWRVC
6ARkaI0OO+Ci2qbEnlJ/GA80V/jLHdERq2u/4dIBDKTZvXWz2oNyvfMMW68A
TExv/km43xltKXMK5HEA9OejJReTXaDvovTvekEgZDs1bgU1ueLqI6OTfzwN
hLnjV6q2g1zkdM+b6gmDsDxDvD5bfS8K4ybCAkaCYNB9qndOh4eUVetUmb7B
GCw6I+Fe5uFXo2WS+BeLPX9/w5CTO+gfFbU7/A8i+0F988yQO6pNr6SWSxfb
srPKOdEDRkG1bvJIERhhtMFWTU8M77nR6P1GhIO0SavbVZ7oq0zawo4LwZzl
irlPHC98anLfwZwKAad34F3ngBcWrsdeyzgkBsO0l1kr8oaLqYN9yowYdlun
WySKfPQITULD4wiY+d2pSYWLn4LJrbIEgsm6V0uWFfPhTypUHh8muN1Ly8or
4eOf2JWNFikE+79kF14o48MgUzYnP0FQlNFS/bCaj7TG9pz8YoKlR2seM1v5
sFHiNVy7QTAWms28/4qPjBVZtPFbBM2ZNdXeUj6eqHd6rb1NkF7dvfnFBB+E
YTGb2kmw7jXNauY9Hz+areE43SfwCUh20Z3l4+O+F/1DIwQSfkR8nIoACNQ2
VHlOUJKQPStXFeBkiMcR9ihBSFFNaqaGAMyYLsaF1wS0vleZZVoCuGfUiCLf
E+zgCst69AU4d3K05edJAuWIZKaHgQDj+Wu/6Jsi+D37bPWIoQBppTl1LBlB
/N2BKx+MBXhw/o5C6CyBnXSalbJVAK2L8x6lnwnU6BqtdDMBgi5ZVfXKCZ4Z
mO4qMBegriFatrBAcHm3W7cOS4D/ALpbLyQ=
        "]]},
      Annotation[#, "Charting`Private`Tag$6267#1"]& ]}, {}, {}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{169.65625, 74.35},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  Method->{"CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Part[{{Identity, Identity}, {Identity, Identity}}, 1, 2][#]& )[
         Part[#, 1]], 
        (Part[{{Identity, Identity}, {Identity, Identity}}, 2, 2][#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Part[{{Identity, Identity}, {Identity, Identity}}, 1, 2][#]& )[
         Part[#, 1]], 
        (Part[{{Identity, Identity}, {Identity, Identity}}, 2, 2][#]& )[
         Part[#, 2]]}& )}},
  PlotRange->{{170., 192.}, {75., 88.}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{
  3.7228643578260794`*^9, {3.7228643934961195`*^9, 3.7228644028196526`*^9}, 
   3.7228644403427987`*^9, {3.7409035972119617`*^9, 3.7409036103077106`*^9}, 
   3.740903785680742*^9}]
}, Open  ]],

Cell[BoxData[","], "Input",
 CellChangeTimes->{3.722864396408286*^9}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"set", "=", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"1", ",", "3"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"2", ",", "5"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"3", ",", "5"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"4", ",", "2"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"5", ",", "6"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"6", ",", "4"}], "}"}]}], "}"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"1", ",", "3"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"2", ",", "5"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"3", ",", "5"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"4", ",", "2"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"5", ",", "6"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"6", ",", "4"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.7408191705236316`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.7408193118057127`*^9, 3.74081934410256*^9}, 
   3.740819429219428*^9, {3.740819514324296*^9, 3.7408195431839466`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"1", ",", "2", ",", "3", ",", "4", ",", "5", ",", "6"}], 
  "}"}]], "Output",
 CellChangeTimes->{3.7408194300324745`*^9, 3.74081951491833*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"3", ",", "5", ",", "5", ",", "2", ",", "6", ",", "4"}], 
  "}"}]], "Output",
 CellChangeTimes->{3.7408194300324745`*^9, 3.74081951492233*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"xTrain", "=", 
  RowBox[{"{", 
   RowBox[{"1", ",", "2", ",", "3", ",", "4", ",", "5", ",", "6"}], 
   "}"}]}]], "Input",
 CellChangeTimes->{{3.7408196203823624`*^9, 3.7408196453517904`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"1", ",", "2", ",", "3", ",", "4", ",", "5", ",", "6"}], 
  "}"}]], "Output",
 CellChangeTimes->{3.7408196470398865`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"yTrain", "=", 
  RowBox[{"{", 
   RowBox[{"3", ",", "5", ",", "5", ",", "2", ",", "6", ",", "4"}], 
   "}"}]}]], "Input",
 CellChangeTimes->{{3.7408196488719916`*^9, 3.7408196573654776`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"3", ",", "5", ",", "5", ",", "2", ",", "6", ",", "4"}], 
  "}"}]], "Output",
 CellChangeTimes->{3.7408196579615116`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"{", 
   RowBox[{"1", ",", "2", ",", "3"}], "}"}], ".", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"1", ",", "2", ",", "3"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"1", ",", "2", ",", "3"}], "}"}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.740819340370346*^9, 3.740819341189393*^9}, {
  3.7408234025426893`*^9, 3.740823429404226*^9}}],

Cell[BoxData[
 TemplateBox[{
  "Dot","dotsh",
   "\"Tensors \\!\\(\\*RowBox[{\\\"{\\\", RowBox[{\\\"1\\\", \\\",\\\", \\\"2\
\\\", \\\",\\\", \\\"3\\\"}], \\\"}\\\"}]\\) and \\!\\(\\*RowBox[{\\\"{\\\", \
RowBox[{RowBox[{\\\"{\\\", RowBox[{\\\"1\\\", \\\",\\\", \\\"2\\\", \
\\\",\\\", \\\"3\\\"}], \\\"}\\\"}], \\\",\\\", RowBox[{\\\"{\\\", RowBox[{\\\
\"1\\\", \\\",\\\", \\\"2\\\", \\\",\\\", \\\"3\\\"}], \\\"}\\\"}]}], \\\"}\\\
\"}]\\) have incompatible shapes.\"",2,33,5,26195927376851043573,"Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{3.740823430158269*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{"{", 
   RowBox[{"1", ",", "2", ",", "3"}], "}"}], ".", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"1", ",", "2", ",", "3"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"1", ",", "2", ",", "3"}], "}"}]}], "}"}]}]], "Output",
 CellChangeTimes->{{3.7408234088750515`*^9, 3.740823430162269*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"w", "=", 
  RowBox[{"{", "1", "}"}]}]], "Input",
 CellChangeTimes->{{3.7408191751668973`*^9, 3.7408191902197585`*^9}, 
   3.740823048584444*^9, {3.740823330822587*^9, 3.740823333330731*^9}}],

Cell[BoxData[
 RowBox[{"{", "1", "}"}]], "Output",
 CellChangeTimes->{3.7408191952920485`*^9, 3.740823049855517*^9, 
  3.740823333951766*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"b", "=", 
  RowBox[{"-", "1"}]}]], "Input",
 CellChangeTimes->{{3.740819203258504*^9, 3.740819204355567*^9}}],

Cell[BoxData[
 RowBox[{"-", "1"}]], "Output",
 CellChangeTimes->{3.740819206177671*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"model", "[", 
   RowBox[{"X_", ",", "w_", ",", "intercept_"}], "]"}], ":=", 
  RowBox[{
   RowBox[{"Dot", "[", 
    RowBox[{"w", ",", "X"}], "]"}], "+", "intercept"}]}]], "Input",
 CellChangeTimes->{{3.7408192428087664`*^9, 3.7408192957987967`*^9}, {
  3.7408193679909263`*^9, 3.7408193887151117`*^9}, {3.740823293873474*^9, 
  3.740823310202408*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"yhat", "=", 
  RowBox[{"model", "[", 
   RowBox[{"w", ",", "xTrain", ",", "b"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7408193962815447`*^9, 3.7408194184358115`*^9}, {
  3.7408194800203342`*^9, 3.7408194831615133`*^9}, {3.740819520573653*^9, 
  3.7408195244778767`*^9}, {3.7408196618377333`*^9, 3.7408196635048285`*^9}, {
  3.740821400322169*^9, 3.740821402209277*^9}, {3.7408233460374575`*^9, 
  3.740823351342761*^9}}],

Cell[BoxData[
 TemplateBox[{
  "Dot","dotsh",
   "\"Tensors \\!\\(\\*RowBox[{\\\"{\\\", RowBox[{\\\"1\\\", \\\",\\\", \\\"2\
\\\", \\\",\\\", \\\"3\\\", \\\",\\\", \\\"4\\\", \\\",\\\", \\\"5\\\", \\\",\
\\\", \\\"6\\\"}], \\\"}\\\"}]\\) and \\!\\(\\*RowBox[{\\\"{\\\", \\\"1\\\", \
\\\"}\\\"}]\\) have incompatible shapes.\"",2,31,4,26195927376851043573,
   "Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{{3.7408233381310053`*^9, 3.7408233518897924`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "1"}], "+", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"1", ",", "2", ",", "3", ",", "4", ",", "5", ",", "6"}], "}"}], 
   ".", 
   RowBox[{"{", "1", "}"}]}]}]], "Output",
 CellChangeTimes->{{3.740819419912896*^9, 3.7408194363578367`*^9}, 
   3.740819483882555*^9, 3.740819525276922*^9, 3.7408196641788673`*^9, 
   3.740821402803311*^9, 3.7408230558968625`*^9, {3.740823305181121*^9, 
   3.740823351964796*^9}}]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"cost", "[", 
   RowBox[{"Yhat_", ",", "Y_"}], "]"}], ":=", 
  RowBox[{"Sum", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"Y", "[", 
        RowBox[{"[", "i", "]"}], "]"}], "-", "Yhat"}], ")"}], "^", "2"}], ",", 
    RowBox[{"{", 
     RowBox[{"i", ",", "1", ",", 
      RowBox[{"Length", "[", "Y", "]"}]}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7408196769936*^9, 3.7408197144387417`*^9}, {
  3.7408213338863688`*^9, 3.7408213879344606`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"cost", "[", 
  RowBox[{"yhat", ",", "yTrain"}], "]"}]], "Input",
 CellChangeTimes->{{3.7408213910156364`*^9, 3.7408214142709665`*^9}}],

Cell[BoxData["44215"], "Output",
 CellChangeTimes->{3.7408214172201357`*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"n", "=", "1"}], ";", 
  RowBox[{
   SubscriptBox["y", "i"], "=", "1"}], ";"}]], "Input",
 CellChangeTimes->{{3.740924040229536*^9, 3.740924058558584*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  SubscriptBox["y", "i"], "=", "2"}]], "Input",
 CellChangeTimes->{{3.740924062493809*^9, 3.740924062625817*^9}}],

Cell[BoxData["2"], "Output",
 CellChangeTimes->{3.7409240638288856`*^9}]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.7408195457140913`*^9, 3.740819613823987*^9}, {
  3.740819666779016*^9, 3.7408196676850677`*^9}}],

Cell[BoxData[
 RowBox[{"Sum", "[", 
  RowBox[{
   TagBox[
    FrameBox["expr"],
    "SelectionPlaceholder"], ",", 
   RowBox[{"{", 
    RowBox[{
     TagBox[
      FrameBox["index"],
      "Placeholder"], ",", 
     TagBox[
      FrameBox["start"],
      "Placeholder"], ",", 
     TagBox[
      FrameBox["end"],
      "Placeholder"]}], "}"}]}], "]"}]], "Input"],

Cell[BoxData[
 StyleBox[
  RowBox[{
   UnderoverscriptBox[
    RowBox[{
     FractionBox["1", "n"], "\[Sum]"}], 
    RowBox[{"       ", 
     RowBox[{"i", "=", "1"}]}], 
    RowBox[{"       ", "n"}]], 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{
      SubscriptBox["y", "i"], "-", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"w", " ", 
         SubscriptBox["x", "i"]}], "+", "b"}], ")"}]}], ")"}], "2"]}],
  FontFamily->"Consolas",
  FontWeight->"Bold"]], "Input",
 CellChangeTimes->{{3.7409238292504683`*^9, 3.740923896501315*^9}, {
  3.740923934808506*^9, 3.740923976524892*^9}}],

Cell[TextData[Cell[BoxData[
 StyleBox[
  RowBox[{
   UnderoverscriptBox[
    RowBox[{
     FractionBox["1", "n"], "\[Sum]"}], 
    RowBox[{"       ", 
     RowBox[{"i", "=", "1"}]}], 
    RowBox[{"       ", "n"}]], 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{
      SubscriptBox["y", "i"], "-", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"w", " ", 
         SubscriptBox["x", "i"]}], "+", "b"}], ")"}]}], ")"}], "2"]}],
  FontFamily->"Consolas",
  FontWeight->"Bold"]],
 CellChangeTimes->{{3.7409238292504683`*^9, 3.740923896501315*^9}, {
  3.740923934808506*^9, 3.740923976524892*^9}}]], "Text",
 CellChangeTimes->{3.7409240867851987`*^9}],

Cell[BoxData[
 StyleBox[
  RowBox[{
   FractionBox["\[PartialD]", 
    RowBox[{"\[PartialD]", "w"}]], 
   RowBox[{"=", 
    RowBox[{
     StyleBox[
      UnderoverscriptBox[
       RowBox[{
        FractionBox[
         RowBox[{"-", "2"}], "n"], "\[Sum]"}], 
       RowBox[{"       ", 
        RowBox[{"i", "=", "1"}]}], 
       RowBox[{"       ", "n"}]],
      FontFamily->"Consolas",
      FontWeight->"Bold"], 
     StyleBox[
      SubscriptBox["x", "i"],
      FontFamily->"Consolas",
      FontWeight->"Bold"], 
     RowBox[{
      StyleBox["(",
       FontFamily->"Consolas",
       FontWeight->"Bold"], 
      StyleBox[
       RowBox[{
        SubscriptBox["y", "i"], "-", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"w", " ", 
           SubscriptBox["x", "i"]}], "+", "b"}], ")"}]}],
       FontFamily->"Consolas",
       FontWeight->"Bold"], 
      StyleBox[")",
       FontFamily->"Consolas",
       FontWeight->"Bold"]}]}]}]}], "Text"]], "Input",
 CellChangeTimes->{{3.740926053229673*^9, 3.7409261211075554`*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["\[PartialD]", 
   RowBox[{"\[PartialD]", "b"}]], 
  RowBox[{"=", 
   StyleBox[
    RowBox[{
     UnderoverscriptBox[
      RowBox[{
       FractionBox[
        RowBox[{"-", "2"}], "n"], "\[Sum]"}], 
      RowBox[{"       ", 
       RowBox[{"i", "=", "1"}]}], 
      RowBox[{"       ", "n"}]], 
     RowBox[{"(", 
      RowBox[{
       SubscriptBox["y", "i"], "-", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"w", " ", 
          SubscriptBox["x", "i"]}], "+", "b"}], ")"}]}], ")"}]}],
    FontFamily->"Consolas",
    FontWeight->"Bold"]}]}]], "Input",
 CellChangeTimes->{{3.7409261382815375`*^9, 3.740926141353713*^9}}],

Cell[TextData[{
 Cell[BoxData[{
  StyleBox[
   RowBox[{
    FractionBox["\[PartialD]", 
     RowBox[{"\[PartialD]", "w"}]], 
    RowBox[{"=", 
     RowBox[{
      StyleBox[
       UnderoverscriptBox[
        RowBox[{
         FractionBox[
          RowBox[{"-", "2"}], "n"], "\[Sum]"}], 
        RowBox[{"       ", 
         RowBox[{"i", "=", "1"}]}], 
        RowBox[{"       ", "n"}]],
       FontFamily->"Consolas",
       FontWeight->"Bold"], 
      StyleBox[
       SubscriptBox["x", "i"],
       FontFamily->"Consolas",
       FontWeight->"Bold"], 
      RowBox[{
       StyleBox["(",
        FontFamily->"Consolas",
        FontWeight->"Bold"], 
       RowBox[{
        StyleBox[
         SubscriptBox["y", "i"],
         FontFamily->"Consolas",
         FontWeight->"Bold"], 
        StyleBox["-",
         FontFamily->"Consolas",
         FontWeight->"Bold"], 
        RowBox[{
         StyleBox["(",
          FontFamily->"Consolas",
          FontWeight->"Bold"], 
         StyleBox[
          RowBox[{
           RowBox[{"w", " ", 
            SubscriptBox["x", "i"]}], "+", "b"}],
          FontFamily->"Consolas",
          FontWeight->"Bold"], 
         StyleBox[")",
          FontFamily->"Consolas",
          FontWeight->"Bold"]}]}], 
       StyleBox[")",
        FontFamily->"Consolas",
        FontWeight->"Bold"]}]}]}]}], "Text"], "\[IndentingNewLine]", 
  StyleBox[
   RowBox[{
    FractionBox["\[PartialD]", 
     RowBox[{"\[PartialD]", "b"}]], 
    RowBox[{"=", 
     RowBox[{
      StyleBox[
       UnderoverscriptBox[
        RowBox[{
         FractionBox[
          RowBox[{"-", "2"}], "n"], "\[Sum]"}], 
        RowBox[{"       ", 
         RowBox[{"i", "=", "1"}]}], 
        RowBox[{"       ", "n"}]],
       FontFamily->"Consolas",
       FontWeight->"Bold"], 
      RowBox[{
       StyleBox["(",
        FontFamily->"Consolas",
        FontWeight->"Bold"], 
       RowBox[{
        StyleBox[
         SubscriptBox["y", "i"],
         FontFamily->"Consolas",
         FontWeight->"Bold"], 
        StyleBox["-",
         FontFamily->"Consolas",
         FontWeight->"Bold"], 
        RowBox[{
         StyleBox["(",
          FontFamily->"Consolas",
          FontWeight->"Bold"], 
         StyleBox[
          RowBox[{
           RowBox[{"w", " ", 
            SubscriptBox["x", "i"]}], "+", "b"}],
          FontFamily->"Consolas",
          FontWeight->"Bold"], 
         StyleBox[")",
          FontFamily->"Consolas",
          FontWeight->"Bold"]}]}], 
       StyleBox[")",
        FontFamily->"Consolas",
        FontWeight->"Bold"]}]}]}]}], "Text"]}],
  CellChangeTimes->{{3.740926053229673*^9, 3.7409261211075554`*^9}}],
 "\n"
}], "Text",
 CellChangeTimes->{{3.740926183498124*^9, 3.740926195057785*^9}, {
  3.740926284186883*^9, 3.7409262852429433`*^9}, {3.7409263778222384`*^9, 
  3.7409263905039635`*^9}}]
},
WindowSize->{1600, 800},
WindowMargins->{{-9, Automatic}, {Automatic, -9}},
Magnification:>1.5 Inherited,
FrontEndVersion->"11.0 for Microsoft Windows (64-bit) (September 21, 2016)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 1020, 31, 104, "Input"],
Cell[CellGroupData[{
Cell[1603, 55, 209, 4, 45, "Input"],
Cell[1815, 61, 1468, 36, 365, "Output"]
}, Open  ]],
Cell[3298, 100, 1095, 29, 74, "Input"],
Cell[CellGroupData[{
Cell[4418, 133, 202, 5, 45, "Input"],
Cell[4623, 140, 745, 23, 63, "Output"]
}, Open  ]],
Cell[5383, 166, 857, 25, 45, "Input"],
Cell[6243, 193, 264, 7, 75, InheritFromParent],
Cell[6510, 202, 96, 1, 45, InheritFromParent],
Cell[6609, 205, 929, 25, 75, "Input"],
Cell[CellGroupData[{
Cell[7563, 234, 248, 5, 45, "Input"],
Cell[7814, 241, 3491, 71, 365, "Output"]
}, Open  ]],
Cell[11320, 315, 69, 1, 45, "Input"],
Cell[CellGroupData[{
Cell[11414, 320, 444, 15, 45, "Input"],
Cell[11861, 337, 451, 15, 45, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12349, 357, 167, 2, 45, "Input"],
Cell[12519, 361, 181, 4, 45, "Output"],
Cell[12703, 367, 181, 4, 45, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12921, 376, 215, 5, 45, "Input"],
Cell[13139, 383, 160, 4, 45, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[13336, 392, 215, 5, 45, "Input"],
Cell[13554, 399, 160, 4, 45, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[13751, 408, 392, 11, 45, "Input"],
Cell[14146, 421, 589, 10, 35, "Message"],
Cell[14738, 433, 344, 10, 45, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15119, 448, 214, 4, 45, "Input"],
Cell[15336, 454, 141, 3, 45, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15514, 462, 133, 3, 45, "Input"],
Cell[15650, 467, 87, 2, 45, "Output"]
}, Open  ]],
Cell[15752, 472, 386, 9, 45, "Input"],
Cell[CellGroupData[{
Cell[16163, 485, 447, 8, 45, "Input"],
Cell[16613, 495, 479, 9, 35, "Message"],
Cell[17095, 506, 448, 11, 45, "Output"]
}, Open  ]],
Cell[17558, 520, 524, 15, 45, "Input"],
Cell[CellGroupData[{
Cell[18107, 539, 158, 3, 45, "Input"],
Cell[18268, 544, 76, 1, 45, "Output"]
}, Open  ]],
Cell[18359, 548, 189, 5, 45, "Input"],
Cell[CellGroupData[{
Cell[18573, 557, 137, 3, 45, "Input"],
Cell[18713, 562, 72, 1, 45, "Output"]
}, Open  ]],
Cell[18800, 566, 145, 2, 45, "Input"],
Cell[18948, 570, 362, 16, 53, "Input"],
Cell[19313, 588, 598, 20, 100, "Input"],
Cell[19914, 610, 657, 21, 87, "Text"],
Cell[20574, 633, 1041, 37, 101, "Input"],
Cell[21618, 672, 670, 23, 100, "Input"],
Cell[22291, 697, 2848, 99, 185, "Text"]
}
]
*)

