import tensorflow as tf
import numpy as np

# To clear the defined variables and operations of the previous cell
tf.reset_default_graph()
# create graph
a = tf.constant(2,name="constant_a")
b = tf.constant(3,name="constant_b")
c = tf.add(a, b,name="addition")
v=tf.constant([[0,1],[1,0],[3,5],[2,5],[10,6]])
v1=tf.constant([[0,1],[1,0],[3,5],[2,5],[10,6]])
# launch the graph in a session
with tf.Session() as sess:
    #writer = tf.summary.FileWriter('./graphs', sess.graph)
    print(sess.run(tf.argmax(v,1)))
    print(sess.run(tf.equal(v,v1)))

