import numpy as np
import matplotlib.pyplot as plt

x_train = np.array([170,175,188,192])
y_train= np.array([75,79,86,88])

def model(x,w,b):
    return x*w+b;

def cost(y,y_hat):
    return np.sum((y-y_hat)**2)/y.size

learning_rate=0.000007
def trainning_round(x_train,y_train,w,b,learning_rate):

    y_hat=model(x_train,w,b)
    print(cost(y_train,y_hat))

    w_gradient=-2*x_train.dot(y_train-y_hat)/y_train.size
    b_gradient=-2*np.sum(y_train-y_hat)/y_train.size

    w-=learning_rate*w_gradient
    b-=learning_rate*b_gradient

    return w,b

num_epoch=100
def train(X,Y):

    w=0
    b=-20

    #for plt
    ar = np.arange(0, 210, 0.5)

    for i in range(num_epoch):
            w,b=trainning_round(X,Y,w,b,learning_rate)

    plt.plot(ar, model(ar,w,b))
    plt.axis([140, 210, 50, 100])
    plt.plot(X, Y, 'ro')

train(x_train,y_train)
plt.show()

