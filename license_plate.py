import os
import numpy as np
from scipy import ndimage
import scipy
import matplotlib.pyplot as plt
from skimage import color
from keras.layers import Input, Dense, Activation
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.layers import Reshape, Lambda
from keras.layers.merge import add, concatenate
from keras.layers.recurrent import GRU
from keras.models import Model
from keras import backend as K
from keras.optimizers import adam
from keras.utils import np_utils
import keras
from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPooling2D, Dropout, Flatten



##
##
#3 doesnt work !!!
##
##

num_px_height = 16
num_px_width = 96

plate_data = []
plate_labels = []

os.chdir('license_data')
i=0

#temp = ndimage.imread("/home/enes/Desktop/ml1/ml/license_data/A00-A-313.png",flatten=False)

for f in os.listdir():
    i+=1
    fname_plate = "/home/enes/Desktop/ml1/ml/license_data/"+str(f)

    #plate_image = np.array(color.rgb2gray(ndimage.imread(fname_plate, flatten=False)))
    plate_image = np.array(ndimage.imread(fname_plate,flatten=False))

    my_image_plate = scipy.misc.imresize(plate_image, size=(num_px_height, num_px_width))

    plate_data.append(my_image_plate)
    temp = f.split(".")
    plate_labels.append(temp[0])

plate_data = np.array(plate_data)
plate_labels = np.array(plate_labels)

print(plate_labels[18])
print(plate_labels[19])
print(plate_data[18].shape)
print(plate_data[18])
plt.imshow(plate_data[18])
#plt.show()
plt.imshow(plate_data[19])
#plt.show()



alphabet = '1234567890AEJKMOT-'

char_to_int = dict((c, i) for i, c in enumerate(alphabet))

plate_labels_encoded=[]

for plate in plate_labels:

    integer_encoded = [char_to_int[char] for char in plate]
    """
    onehot_encoded = list()
    for value in integer_encoded:
	    letter = [0 for _ in range(len(alphabet))]
	    letter[value] = 1
	    onehot_encoded.append(letter)

    print(plate)
    print(onehot_encoded)
    """
    plate_labels_encoded.append(integer_encoded)

plate_labels_encoded=np.array(plate_labels_encoded)

temp_num = int((i*9)/10)
print(temp_num)
training_data = plate_data[:temp_num]
training_labels = plate_labels_encoded[:temp_num]

validation_data = plate_data[temp_num:]
validation_labels = plate_labels_encoded[temp_num:]

print(training_data.shape)
model = Sequential()
model.add(Conv2D(32, (3, 3), padding='same', activation='relu', input_shape=(num_px_height,num_px_width,3)))
#model.add(Conv2D(32, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(1, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
model.add(Conv2D(64, (3, 3), padding='same',activation='relu'))
model.add(MaxPooling2D(pool_size=(1, 2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(32, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(9, activation='linear'))

opt = adam(lr=0.001)

model.compile(optimizer=opt, loss='mean_squared_logarithmic_error', metrics=['categorical_accuracy'])
model.summary()

history = model.fit(training_data,training_labels,batch_size=2, epochs=100,validation_data=(validation_data, validation_labels))
print(model.predict(training_data,2))
print(training_labels)
"""

conv_filters = 16
kernel_size = (3, 3)
pool_size = 2
time_dense_size = 32
rnn_size = 512
minibatch_size = 16
unique_tokens = 18

if K.image_data_format() == 'channels_first':
    input_shape = (1, num_px_height, num_px_width)
else:
    input_shape = (num_px_height, num_px_width, 1)

act='relu'

input_data = Input(name='the_input', shape=input_shape, dtype='float32')
inner = Conv2D(conv_filters, kernel_size, padding='same',
               activation=act, kernel_initializer='he_normal',
               name='conv1')(input_data)
inner = MaxPooling2D(pool_size=(pool_size, pool_size), name='max1')(inner)
inner = Conv2D(conv_filters, kernel_size, padding='same',
               activation=act, kernel_initializer='he_normal',
               name='conv2')(inner)
inner = MaxPooling2D(pool_size=(pool_size, pool_size), name='max2')(inner)

conv_to_rnn_dims = (num_px_width // (pool_size ** 2), (num_px_height // (pool_size ** 2)) * conv_filters)
inner = Reshape(target_shape=conv_to_rnn_dims, name='reshape')(inner)
# cuts down input size going into RNN:
inner = Dense(time_dense_size, activation=act, name='dense1')(inner)
# Two layers of bidirectional GRUs
# GRU seems to work as well, if not better than LSTM:
gru_1 = GRU(rnn_size, return_sequences=True, kernel_initializer='he_normal', name='gru1')(inner)
gru_1b = GRU(rnn_size, return_sequences=True, go_backwards=True, kernel_initializer='he_normal', name='gru1_b')(inner)
gru1_merged = add([gru_1, gru_1b])
gru_2 = GRU(rnn_size, return_sequences=True, kernel_initializer='he_normal', name='gru2')(gru1_merged)
gru_2b = GRU(rnn_size, return_sequences=True, go_backwards=True, kernel_initializer='he_normal', name='gru2_b')(gru1_merged)

# transforms RNN output to character activations:
inner = Dense(unique_tokens, kernel_initializer='he_normal',
              name='dense2')(concatenate([gru_2, gru_2b]))

y_pred = Activation('softmax', name='softmax')(inner)
Model(inputs=input_data, outputs=y_pred)

labels = Input(name='the_labels', shape=[unique_tokens], dtype='float32')
input_length = Input(name='input_length', shape=[1], dtype='int64')
label_length = Input(name='label_length', shape=[1], dtype='int64')

def ctc_lambda_func(args):
    y_pred, labels, input_length, label_length = args
    # the 2 is critical here since the first couple outputs of the RNN
    # tend to be garbage:
    y_pred = y_pred[:, 2:, :]
    return K.ctc_batch_cost(labels, y_pred, input_length, label_length)

loss_out = Lambda(ctc_lambda_func, output_shape=(1,), name='ctc')([y_pred, labels, input_length, label_length])

adam = adam(lr=0.01)

model = Model(inputs=[input_data, labels, input_length, label_length], outputs=loss_out)

model.compile(loss={'ctc': lambda y_true, y_pred: y_pred}, optimizer=adam)

test_func = K.function([input_data], [y_pred])

#viz_cb = VizCallback(run_name, test_func, img_gen.next_val())

model.fit(plate_data,plate_labels,batch_size=250,epochs=50,validation_data=(validation_data, validation_labels))

"""





