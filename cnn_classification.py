import numpy as np
import scipy
from scipy import ndimage
import matplotlib.pyplot as plt
from sklearn.utils import shuffle
from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPooling2D, Dropout, Flatten
from keras.utils.np_utils import to_categorical

num_px=32
num_data=12000

train_set_x_orig=[]
test_set_x_orig=[]

cat_data = []
cat_data_labels = []
dog_data = []
dog_data_labels = []

for i in range (1,num_data):

    myImage = str(i) + ".jpg"
    fname_cat = "Cat/"+myImage
    fname_dog = "Dog/"+myImage
    cat_image = np.array(ndimage.imread(fname_cat, flatten=False))
    print(i)
    dog_image = np.array(ndimage.imread(fname_dog, flatten=False))
    print(i)
    my_image_cat = scipy.misc.imresize(cat_image, size=(num_px, num_px))
    my_image_dog = scipy.misc.imresize(dog_image, size=(num_px, num_px))
    if(my_image_cat.shape==(32, 32, 3)):
        cat_data.append(my_image_cat)
        #print(my_image_cat.shape)
        cat_data_labels.append(0)
    if(my_image_dog.shape==(32, 32, 3)):
        dog_data.append(my_image_dog)
        #print(my_image_dog.shape)
        dog_data_labels.append(1)


cat_data = np.array(cat_data)
cat_data_labels = np.array(cat_data_labels)
dog_data = np.array(dog_data)
dog_data_labels = np.array(dog_data_labels)

data = np.concatenate((cat_data, dog_data), axis=0)
data_labels = np.concatenate((cat_data_labels, dog_data_labels),axis=0)

"""
print(data_labels)
plt.imshow(data[18])
plt.show()
plt.imshow(data[19])
plt.show()
"""

data, data_labels = shuffle(data, data_labels)

categorical_labels = to_categorical(data_labels, num_classes=2)

# split data set

temp_num = int((num_data*2*9)/10)
print(temp_num)
training_data = data[:temp_num]
training_labels = categorical_labels[:temp_num]

validation_data = data[temp_num:]
validation_labels = categorical_labels[temp_num:]

print(training_data.shape)
print(validation_data.shape)

"""
print(categorical_labels)
plt.imshow(data[0])
plt.show()
plt.imshow(data[1])
plt.show()
plt.imshow(data[2])
plt.show()
"""


model = Sequential()
model.add(Conv2D(32, (3, 3), padding='same', activation='relu', input_shape=(num_px,num_px,3)))
model.add(Conv2D(32, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(512, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(2, activation='sigmoid'))

from keras.optimizers import adam
opt = adam(lr=0.002)

model.compile(optimizer=opt, loss='binary_crossentropy', metrics=['accuracy'])

history = model.fit(training_data,training_labels,batch_size=600, epochs=100,validation_data=(validation_data, validation_labels))
model.evaluate(data,categorical_labels)
