import numpy as np

x_train = np.array([[450,20,2],[300,12,3],[360,14,3]])
y_train= np.array([345000,580000,360000])

def model(x,w,b):
    return x.dot(w)+b;

def cost(y,y_hat):
    return np.sum((y-y_hat)**2)/y.size

learning_rate=0.0000002
def trainning_round(x_train,y_train,w,b,learning_rate):

    y_hat=model(x_train,w,b)
    print("greška",cost(y_train,y_hat))

    w_gradient=-2*x_train.dot(y_train-y_hat)/y_train.size
    b_gradient=-2*np.sum(y_train-y_hat)/y_train.size

    w-=learning_rate*w_gradient
    b-=learning_rate*b_gradient

    return w,b

num_epoch=1000
def train(X,Y):

    w=np.random.rand(X[0].size)
    b=-20
    for i in range(num_epoch):
        for(x,y) in zip(X,Y):
            w,b=trainning_round(x,y,w,b,learning_rate)

    print("vrijednost objekta sa parametrima 300, 20, 2  je: ", model(np.array([300, 20, 2]), w, b))

train(x_train,y_train)
