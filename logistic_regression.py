import numpy as np
import sklearn.datasets
import matplotlib.pyplot as plt

iris = sklearn.datasets.load_iris()
features = iris.data[:, :2]
target = (iris.target != 0) * 1
print(features,target)
plt.scatter(features[target == 0][:, 0], features[target == 0][:, 1], color='b')
plt.scatter(features[target == 1][:, 0], features[target == 1][:, 1], color='r')

weights = np.zeros(features.shape[1]+1)

def add_intercept(X):
    intercept = np.ones((X.shape[0],1))
    return np.concatenate((intercept,X),axis=1)

features=add_intercept(features)

def sigmoid(z):
    return 1/(1+np.exp(-z))

def loss(h,target):
    return (-target * np.log(h) - (1 - target) * np.log(1 - h)).mean()
def model(weights,arg):
    return (weights[0]+arg*weights[1])/(-weights[2])

learning_rate=0.1
num_iter=200000

for i in range(num_iter):
    z=np.dot(features,weights)
    h=sigmoid(z)
    print(loss(h, target))

    gradient=np.dot(features.T, (h - target)) / target.size
    weights-=learning_rate*gradient

    #for plt
    ar = np.arange(0, 40, 0.5)
    if i%20000==0 or i<10:
        plt.plot(ar, model(weights, ar))
    if(i==num_iter-1):
        plt.plot(ar, model(weights, ar),linewidth='3',color='r')
plt.axis([4, 8, 2, 5])
plt.show()
