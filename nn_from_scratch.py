import numpy as np
import matplotlib.pyplot as plt


#number of points
n_data=250

one_fourth=int(n_data/4)
half=int(n_data/2)
three_fourth=int(3*n_data/4)

#creating points for XOR problem
data=np.random.rand(n_data,2)
data[:half,0]=data[:half,0]*3
data[half:,0]=data[half:,0]*3+4
data[:one_fourth,1]=data[:one_fourth,1]*3
data[one_fourth:half,1]=data[one_fourth:half,1]*3+4
data[half:three_fourth,1]=data[half:three_fourth,1]*3
data[three_fourth:,1]=data[three_fourth:,1]*3+4

#labels for data
target=np.zeros(n_data)
target[one_fourth:three_fourth]=target[one_fourth:three_fourth]+1

#transpose data
data=data.T

#plot
plt.scatter(data.T[target == 0][:, 0], data.T[target == 0][:, 1], color='b')
plt.scatter(data.T[target == 1][:, 0], data.T[target == 1][:, 1], color='r')
#plt.show()

# size of input layer
n_x=data.shape[0]
# size of hidden layer
n_h=3
# size of output layer
n_y=1

w1=np.random.rand(n_h,n_x)
b1=np.zeros((n_h,1))
w2=np.random.rand(n_y,n_h)
b2=0

def sigmoid(z):
    return 1/(1+np.exp(-z))

def loss(y_hat,target):
    return np.sum((target-y_hat)**2)/target.size

def forward_propagation(w1,w2,b1,b2,data):
    z1 = np.dot(w1, data) + b1
    a1 = np.tanh(z1)
    z2 = np.dot(w2, a1) + b2
    a2 = sigmoid(z2)
    return a1,a2

learning_rate=0.008
num_iter=2000
for i in range(num_iter):
    #forward propagation
    a1,a2=forward_propagation(w1,w2,b1,b2,data)

    print(loss(a2,target))
    #backward propagation
    dz2=(a2-target)
    dw2=np.dot(dz2,a1.T)/n_x
    db2=np.sum(dz2,axis=1,keepdims=True)/n_x
    dz1=np.dot(w2.T,dz2)*(1 - np.power(a1, 2))
    dw1=np.dot(dz1,data.T)/n_x
    db1=np.sum(dz1,axis=1,keepdims=True)/n_x

    w2-=learning_rate*dw2
    b2-=learning_rate*db2
    w1-=learning_rate*dw1
    b1-=learning_rate*db1



# Set min and max values and give it some padding
x_min, x_max = data[0, :].min() - 1, data[0, :].max() + 1
y_min, y_max = data[1, :].min() - 1, data[1, :].max() + 1
h = 0.05

# Generate a grid of points with distance h between them
xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
test=np.array([xx.ravel(),yy.ravel()])

#test
_,a2=forward_propagation(w1,w2,b1,b2,test)

#plot
plt.scatter(test.T[a2[0] > 0.5][:,0], test.T[a2[0] > 0.5][:,1], color='w')
plt.scatter(test.T[a2[0] <= 0.5][:,0], test.T[a2[0] <= 0.5][:,1], color='#fff205')
plt.scatter(data.T[target == 0][:, 0], data.T[target == 0][:, 1], color='b')
plt.scatter(data.T[target == 1][:, 0], data.T[target == 1][:, 1], color='r')
plt.show()

