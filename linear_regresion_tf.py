import tensorflow as tf
import numpy as np
tf.reset_default_graph()

# [number of houses ,features]
X = tf.placeholder(tf.float32,[None,3])
Y = tf.placeholder(tf.float32,[None,1])

W=tf.get_variable("weights",[3,1],initializer=tf.random_normal_initializer)
b=tf.get_variable("bias",[1],initializer=tf.constant_initializer(0))

Y_hat = tf.matmul(X,W)+b
cost = tf.reduce_mean(tf.square(Y_hat-Y))

learning_rate=0.000006
optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize((cost))

cost_summary = tf.summary.scalar(name='cost_without_norm', tensor=cost)
histogram_summary = tf.summary.histogram('Histogram_summary', W)

num_epochs=10000
with tf.Session() as sess:
    #create writer
    writer = tf.summary.FileWriter('./graphs', sess.graph)
    #initialize variables
    sess.run(tf.global_variables_initializer())

    data_X=np.array([[450.,20.,2.],[300.,12.,3.],[360.,14.,3.]])

    data_Y=np.array([[345000,580000,360000]]).T
    #train
    for i in range(num_epochs):
        sess.run(optimizer,
                feed_dict={X:data_X,
                           Y:data_Y})

        #evaluate the scalar summary
        summary1 = sess.run(cost_summary,feed_dict={X:data_X,
                           Y:data_Y})
        # evaluate the scalar summary
        summary2 = sess.run(histogram_summary)
        #add summary1 and summary2 to the writer
        writer.add_summary(summary1, i)
        writer.add_summary(summary2, i)

    print(sess.run(Y_hat,feed_dict={X:np.array([[450,20,2]])}))