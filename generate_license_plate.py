from PIL import ImageFont, ImageDraw, Image
import numpy as np
import cv2
import random

font = ImageFont.truetype("/home/enes/Desktop/font/EuroPlate.ttf", 140)
font1 = ImageFont.truetype("/home/enes/Desktop/font/EuroPlate.ttf", 40)

words = ["A","E",'J','K','M','O','T']
for i in range(5000):
    num1= random.randint(0,99)

    if num1 < 10:
        num1 = str(0)+str(num1)
    else:
        num1 = str(num1)

    num2 = random.randint(0,999)

    if num2 < 10:
        num2 = str(0)+str(0)+str(num2)
    elif num2 >=10 and num2 < 100:
        num2 = str(0)+ str(num2)
    else:
        num2 = str(num2)

    number_plate = words[random.randint(0,6)]+num1+'-'+words[random.randint(0,6)]+'-'+num2

    print(number_plate)

    img = np.zeros((128, 740, 3), np.uint8)
    pil_img = Image.fromarray(img)
    draw = ImageDraw.Draw(pil_img)

    draw.text((80, 5), number_plate, font=font)



    cv2_img = cv2.cvtColor(np.array(pil_img), cv2.COLOR_RGB2BGR)
    cv2_img = cv2.bitwise_not(cv2_img)
    cv2.rectangle(cv2_img,(0,0),(70,128),(255,0,0),-1)
    cv2.putText(cv2_img,'BIH',(-1,120),cv2.FONT_HERSHEY_SIMPLEX,1.4,(255,255,255),4)



    cv2.imwrite(r"license_data/" + number_plate +  ".png", cv2_img)