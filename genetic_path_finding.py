import pygame as pg
from random import randint, uniform,random
import bisect
import numpy as np

vec = pg.math.Vector2

WIDTH = 1200
HEIGHT = 600
RESOLUTION =20
FPS = 60
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
DARKGRAY = (40, 40, 40)


# Mob properties
MOB_SIZE = 10
MAX_SPEED = 10
MAX_FORCE = 0.1

# Genetic properties
mutation_rate = 0.015
population_num = 350

class Population:
    def __init__(self,target):
        self.target = target
        self.population = []
        self.generation = 0

    def add_mob(self,mob):
        self.population.append(mob)
    def calc_fitness(self):
        sum = 0
        for m in self.population:
            m.calc_fitness()
            sum+=m.fitness
        for m in self.population:
            m.fitness/=sum
    def selection(self):
        new_population = []
        reproduction = []
        result = []
        sum = 0
        for e in self.population:
            sum+= e.fitness
            result.append(sum)
        #print("suma ",sum)
        for i in range(population_num):
            x = uniform(0,0.999999)
            idx = bisect.bisect_left(result,x)
            #print(idx)
            if idx == 50:
                idx-=1
            reproduction.append(self.population[idx])
            #print(self.population[idx].path)

        for i in range(population_num-1):
            elem = reproduction[i].crossover(reproduction[i+1])
            #print(elem.path)
            new_population.append(elem)
            #print(i," ",new_population[i].path)

        elem = reproduction[0].crossover(reproduction[population_num-1])
        new_population.append(elem)
        for i in range(population_num):
            #print("prije i ",i," ",new_population[i].path)
            new_population[i].mutate()
            #print("poslije",new_population[i].path)
            new_population[i].k=0
            new_population[i].stopped=0
        self.population = new_population
        self.generation +=1

class FTarget(pg.sprite.Sprite):
    def __init__(self):
        #self.groups = target_sprite
        pg.sprite.Sprite.__init__(self)
        self.image = pg.Surface((30,30))
        self.image.fill(RED)
        self.rect = self.image.get_rect()
        self.pos = vec(1100,HEIGHT/2)
        self.rect.center = self.pos

class Rectangle(pg.sprite.Sprite):
    def __init__(self,topleft_x,topleft_y, width, height):
        #self.groups = target_sprite
        pg.sprite.Sprite.__init__(self)
        self.image = pg.Surface((width,height))
        self.image.fill(BLUE)
        self.rect = self.image.get_rect()
        #self.pos = vec(1100,HEIGHT/2)
        self.rect.topleft = (topleft_x,topleft_y)


class Mob(pg.sprite.Sprite):
    def __init__(self,f_target):
        #self.groups = all_sprites
        pg.sprite.Sprite.__init__(self)
        self.image = pg.Surface((MOB_SIZE, MOB_SIZE))
        self.image.fill(YELLOW)
        self.rect = self.image.get_rect()
        #self.pos = vec(randint(0, WIDTH), randint(0, HEIGHT))
        self.resolution = RESOLUTION
        self.flowfield = np.zeros((int(HEIGHT/RESOLUTION),int(WIDTH/RESOLUTION)),dtype=object)
        self.flowfield_height = len(self.flowfield)
        self.flowfield_width = len(self.flowfield[0])
        self.pos = vec(20,HEIGHT/2)
        self.vel = vec(0, 0)#.rotate(uniform(-90, 90))
        self.acc = vec(0, 0)
        self.rect.center = self.pos
        self.fitness_calculated=0
        self.generation = 0
        self.target = f_target
        self.fitness = 0
        self.stopped = 0

    def create_flowfield(self):
        for i in range(len(self.flowfield)):
            for j in range(len(self.flowfield[0])):
                self.flowfield[i,j] = vec(uniform(-MAX_SPEED,MAX_SPEED),uniform(-MAX_SPEED,MAX_SPEED)).rotate(uniform(0,360))


    def crossover(self,partner):
        mob = Mob(self.target)
        midpoint = randint(1, self.flowfield_width)
        for i in range(self.flowfield_height):
            for j in range(self.flowfield_width):
                if j < midpoint:
                    mob.flowfield[i,j] = self.flowfield[i,j]
                else:
                    mob.flowfield[i,j] = partner.flowfield[i,j]

        return mob

    def mutate(self):
        for i in range(self.flowfield_height):
            for j in range(self.flowfield_width):
                if j < random():
                    self.flowfield[i,j].rotate(uniform(0,360))

    def calc_fitness(self):
        x_pos = self.pos.x
        y_pos = self.pos.y
        f = self.flowfield[int(y_pos/RESOLUTION),int(x_pos/RESOLUTION)].distance_to(self.target.pos)
        c = 1

        if f!=0:
            """
            if f>1000:
                c=0.01
            elif f<=1000 and f>700:
                c=0.1
            elif f<=700 and f>400:
                c=0.5
            """
            f = (1 / f) * 100
            self.fitness = f*c
        else:
            self.fitness = 100
        if self.fitness_calculated==1:
            self.fitness=100
            self.fitness_calculated=0

    def collided(self):
        self.acc = vec(0, 0)
        self.vel = vec(0, 0)
        self.stopped = 1
        self.fitness = 100
        self.image.fill(RED)
        self.fitness_calculated=1

    def lookup(self):
        x_pos = self.pos.x
        y_pos = self.pos.y

        desired = self.flowfield[int(y_pos/RESOLUTION),int(x_pos/RESOLUTION)]
        return desired

    def seek(self):
        #self.desired = (target-self.pos).normalize()* randint(1,MAX_SPEED)
        desired = self.lookup()#*randint(1,MAX_SPEED)
        steer = (desired-self.vel)
        if steer.length() > MAX_FORCE:
            steer.scale_to_length(MAX_FORCE)
        return steer
    def update(self):
        # self.follow_mouse()
        #self.acc = self.seek_with_approach(pg.mouse.get_pos())
        self.acc = self.seek()
        # equations of motion
        self.vel += self.acc
        if self.vel.length() > MAX_SPEED:
            self.vel.scale_to_length(MAX_SPEED)
        self.pos+=self.vel
        if self.pos.x >= WIDTH:
            self.pos.x = WIDTH-5
        if self.pos.x < 0:
            self.pos.x = 0
        if self.pos.y >= HEIGHT:
            self.pos.y = HEIGHT-5
        if self.pos.y < 0:
            self.pos.y = 0
        self.rect.center = self.pos


pg.init()
FONT = pg.font.SysFont("Consolas", 20)
screen = pg.display.set_mode((WIDTH, HEIGHT))
clock = pg.time.Clock()

all_sprites = pg.sprite.Group()
target_sprite = pg.sprite.Group()
rectangle_sprites = pg.sprite.Group()
f = FTarget()
target_sprite.add(f)
p = Population(f)

for i in range(population_num):
    m=Mob(f)
    m.create_flowfield()
    all_sprites.add(m)
    p.add_mob(m)
    #print(m.calc_fitness())
#p.calc_fitness()

paused = True
running = True
mousepos = None
rectangle_index= 0

start_time = pg.time.get_ticks()

while running:
    clock.tick(FPS)
    for event in pg.event.get():
        if event.type == pg.MOUSEBUTTONDOWN:
            mousepos = [event.pos[0], event.pos[1], 0, 0]
        if event.type == pg.MOUSEBUTTONUP:
            r = Rectangle(mousepos[0],mousepos[1],mousepos[2],mousepos[3])
            rectangle_sprites.add(r)
            mousepos = None
        if event.type == pg.MOUSEMOTION and mousepos != None:
            mousepos = [mousepos[0], mousepos[1], event.pos[0] - mousepos[0], event.pos[1] - mousepos[1]]
        if event.type == pg.QUIT:
            running = False
        if event.type == pg.KEYDOWN:
            if event.key == pg.K_ESCAPE:
                running = False
            if event.key == pg.K_SPACE:
                paused = not paused
    if not paused:
        all_sprites.update()

    collided_mobs=pg.sprite.spritecollide(f,all_sprites,False)
    for m in collided_mobs:
        m.collided()
    if len(rectangle_sprites)>0:
        collided_with_block = pg.sprite.groupcollide(all_sprites,rectangle_sprites,False,False)
        for m in collided_with_block:
            m.vel = m.vel*(-1)

    pg.display.set_caption("{:.2f}".format(clock.get_fps()))
    screen.fill(DARKGRAY)
    all_sprites.draw(screen)
    if start_time:
        time_since_enter = (pg.time.get_ticks() - start_time)/1000
        message = 'Seconds since enter: ' + str(time_since_enter)
        message2 = 'Generation: '+ str(p.generation)

        screen.blit(FONT.render(message, True, WHITE), (0, 0))
        screen.blit(FONT.render(message2, True, WHITE), (0, 20))
        if time_since_enter > 15 and not paused:
            start_time = pg.time.get_ticks()
            p.calc_fitness()
            p.selection()
            for mob in all_sprites:
                mob.kill()
            for i in range(population_num):
                all_sprites.add(p.population[i])
                # print(p.population[i].path)

    target_sprite.draw(screen)
    rectangle_sprites.draw(screen)
    if mousepos != None:
        pg.draw.rect(screen, BLUE, mousepos)
    pg.display.flip()

