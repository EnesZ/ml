import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

# import MNIST data
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

learning_rate=0.1
epoch=10
batch_size=100
# frequency of displaying the training results
display_freq = 100

#  MNIST images are 28x28 pixels
img_h = img_w = 28
# images are stored in one-dimensional arrays of this length.
img_size_flat = img_h * img_w
# number of classes, one class for each of 10 digits.
n_classes = 10
# number of units in the first hidden layer
h1 = 200

#input
x=tf.placeholder(tf.float32,shape=[None,img_size_flat],name='X')
y=tf.placeholder(tf.float32,shape=[None,n_classes],name='Y')

#weights
init=tf.truncated_normal_initializer(stddev=0.01)
w1=tf.get_variable(name='weights1',shape=[img_size_flat,h1],dtype=tf.float32,initializer=init)
b1=tf.get_variable(name='bias1',dtype=tf.float32,initializer=tf.constant(0.,shape=[h1],dtype=tf.float32))
w2=tf.get_variable(name='weights2',shape=[h1,n_classes],initializer=init)
b2=tf.get_variable(name='bias2',dtype=tf.float32,initializer=tf.constant(0.,shape=[n_classes],dtype=tf.float32))

#hidden layer
h_layer=tf.matmul(x,w1)+b1
h_layer=tf.nn.relu(h_layer)

#output layer
o_layer=tf.matmul(h_layer,w2)+b2

# loss and optimizer
loss=tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=y,logits=o_layer),name='loss')
optimizer=tf.train.GradientDescentOptimizer(learning_rate,name="GDO").minimize(loss)

# accuracy
correct_prediction = tf.equal(tf.argmax(o_layer, axis=1), tf.argmax(y, axis=1), name='correct_pred')
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name='accuracy')
# network predictions
cls_prediction = tf.argmax(o_layer, axis=1, name='predictions')


# to keep the parameters we use InteractiveSession
sess=tf.InteractiveSession()
# Initializing the variables
sess.run(tf.global_variables_initializer())

# create writer
#writer = tf.summary.FileWriter('./graphs', sess.graph)

# number of batches
num_iter=int(mnist.train.num_examples/batch_size)

for i in range(epoch):

    print("Epoch {}".format(i))
    for iteration in range(num_iter):

        batch_x,batch_y=mnist.train.next_batch(batch_size)
        # optimization - backpropagation
        feed_dict_batch={x:batch_x,y:batch_y}
        sess.run(optimizer,feed_dict=feed_dict_batch)

        if iteration % display_freq == 0:

            loss_batch, acc_batch= sess.run([loss,accuracy],feed_dict=feed_dict_batch)

            print("iter {0}:\t Loss={1},\tTraining Accuracy={2:.01%}".
                      format(iteration, loss_batch, acc_batch))

    # Run validation after every epoch
    feed_dict_valid = {x: mnist.validation.images, y: mnist.validation.labels}
    loss_valid, acc_valid = sess.run([loss, accuracy], feed_dict=feed_dict_valid)
    print('---------------------------------------------------------')
    print("Epoch: {0}, validation loss: {1:.2f}, validation accuracy: {2:.01%}".
                  format(i + 1, loss_valid, acc_valid))
    print('---------------------------------------------------------')

def plot_images(images, cls_true, cls_pred=None, title=None):
    fig, axes = plt.subplots(3, 3, figsize=(9, 9))
    fig.subplots_adjust(hspace=0.3, wspace=0.3)
    img_h = img_w = np.sqrt(images.shape[-1]).astype(int)
    for i, ax in enumerate(axes.flat):
        # Plot image.
        ax.imshow(images[i].reshape((img_h, img_w)), cmap='binary')

        # Show true and predicted classes.
        if cls_pred is None:
            ax_title = "True: {0}".format(cls_true[i])
        else:
            ax_title = "True: {0}, Pred: {1}".format(cls_true[i], cls_pred[i])

        ax.set_title(ax_title)

        # Remove ticks from the plot.
        ax.set_xticks([])
        ax.set_yticks([])

    if title:
        plt.suptitle(title, size=20)
    plt.show()

def plot_example_errors(images, cls_true, cls_pred, title=None):
    # Negate the boolean array.
    incorrect = np.logical_not(np.equal(cls_pred, cls_true))

    # Get the images from the test-set that have been
    # incorrectly classified.
    incorrect_images = images[incorrect]

    # Get the true and predicted classes for those images.
    cls_pred = cls_pred[incorrect]
    cls_true = cls_true[incorrect]

    # Plot the first 9 images.
    plot_images(images=incorrect_images[0:9],
                cls_true=cls_true[0:9],
                cls_pred=cls_pred[0:9],
                title=title)

# Test the network after training
# Accuracy
feed_dict_test = {x: mnist.test.images, y: mnist.test.labels}
loss_test, acc_test = sess.run([loss, accuracy], feed_dict=feed_dict_test)
print('---------------------------------------------------------')
print("Test loss: {0:.2f}, test accuracy: {1:.01%}".format(loss_test, acc_test))
print('---------------------------------------------------------')

# Plot some of the correct and misclassified examples
cls_pred = sess.run(cls_prediction, feed_dict=feed_dict_test)
cls_true = np.argmax(mnist.test.labels, axis=1)
plot_images(mnist.test.images, cls_true, cls_pred, title='Correct Examples')
plot_example_errors(mnist.test.images, cls_true, cls_pred, title='Misclassified Examples')

sess.close()

