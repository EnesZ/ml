import random
import bisect

goal = "hello world"
goal_ascii = []
for c in goal:
    goal_ascii.append(ord(c))

alphabet = []

for i in range(97,123):
    alphabet.append(i)
alphabet.append(32)

mutation = 0.015
population_num = 100

class Population:
    def __init__(self, target, mutation, max_population):
        self.target = target
        self.target_ascii = []
        for ch in target:
            self.target_ascii.append(ord(ch))

        self.mutation = mutation
        self.max_population = max_population
        self.generation = 0
        self.population = []
        self.stop = 0
        self.target_len = len(target)
        for i in range(max_population):
            self.population.append(Element(self.target_len))

    def calc_fitness(self):
        sum = 0
        for e in self.population:
            e.calc_fitness()
            sum +=e.fitness

        for e in self.population:
            e.fitness/=sum

    def top_population(self):
        print("Generation: ",self.generation)
        top = self.population[0]
        for e in self.population:
            if e.real_fitness == self.target_len:
                self.stop=1
            if e.fitness > top.fitness:
                top = e
        print("Top element in population is ", top," with fitness ", top.real_fitness/self.target_len,"%")

    def selection(self):
        new_population = []
        reproduction = []
        result = []
        sum = 0
        for e in self.population:
            sum+= e.fitness
            result.append(sum)
        for i in range(self.max_population):
            x = random.uniform(0,0.999)
            idx = bisect.bisect_left(result,x)
            reproduction.append(self.population[idx])
        for i in range(self.max_population-1):
            elem = reproduction[i].crossover(reproduction[i+1])
            new_population.append(elem)
        elem = reproduction[0].crossover(reproduction[self.max_population-1])
        new_population.append(elem)

        for i in range(self.max_population):
            new_population[i].mutate(self.mutation)
        self.population = new_population
        self.generation +=1

class Element:
    def __init__(self,length):
        self.length = length
        self.string = random.sample(alphabet,length)
        self.fitness = 0
        self.real_fitness = 0

    def __str__(self):
        return "".join(map(chr,self.string))

    def calc_fitness(self):
        for i in range(self.length):
            if goal_ascii[i]==self.string[i]:
                self.fitness+=1
        self.real_fitness = self.fitness
        #self.fitness/=self.length

    def crossover(self,partner):

        elem = Element(self.length)
        midpoint = random.randint(1,self.length)

        for i in range(self.length):
            if i < midpoint:
                elem.string[i]=self.string[i]
            else:
                elem.string[i] = partner.string[i]

        return elem

    def mutate(self,mutation_rate):
        for i in range(self.length):
            if random.random() < mutation_rate:
                self.string[i]=random.choice(alphabet)

x = Element(len(goal))
y = Element(len(goal))

population = Population(goal,mutation,200)
for i in range(500):
    population.calc_fitness()
    population.top_population()
    population.selection()
    if population.stop==1:
        break



