import pygame as pg
from random import randint, uniform,random
import bisect

vec = pg.math.Vector2

WIDTH = 1200
HEIGHT = 600
FPS = 60
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
DARKGRAY = (40, 40, 40)

# Mob properties
MOB_SIZE = 10
MAX_SPEED = 10
MAX_FORCE = 0.2

# Genetic properties
mutation_rate = 0.015
population_num = 150
path_size = 20



path_list = []

for i in range(population_num):
    path = [vec(200, HEIGHT/2)]
    for j in range(1,path_size):
        path.append(path[j-1]+vec(randint(-5,5),randint(-5,5)))
    path_list.append(path)

class Population:
    def __init__(self,target):
        self.target = target
        self.population = []
        self.generation = 0
        self.stopped = 0
    def is_stopped(self):
        stopped = 1
        for mob in self.population:
            if mob.stopped==0:
                stopped=0
                break
        self.stopped=stopped

    def add_mob(self,mob):
        self.population.append(mob)
    def calc_fitness(self):
        sum = 0
        for m in self.population:
            m.calc_fitness()
            sum+=m.fitness
        for m in self.population:
            m.fitness/=sum
    def selection(self):
        new_population = []
        reproduction = []
        result = []
        sum = 0
        for e in self.population:
            sum+= e.fitness
            result.append(sum)
        print("suma ",sum)
        for i in range(population_num):
            x = uniform(0,0.999999)
            idx = bisect.bisect_left(result,x)
            print(idx)
            if idx == 50:
                idx-=1
            reproduction.append(self.population[idx])
            #print(self.population[idx].path)

        for i in range(population_num-1):
            elem = reproduction[i].crossover(reproduction[i+1])
            #print(elem.path)
            new_population.append(elem)
            #print(i," ",new_population[i].path)

        elem = reproduction[0].crossover(reproduction[population_num-1])
        new_population.append(elem)
        for i in range(population_num):
            print("prije i ",i," ",new_population[i].path)
            new_population[i].mutate()
            #print("poslije",new_population[i].path)
            new_population[i].k=0
            new_population[i].stopped=0
        self.population = new_population
        self.generation +=1
    """
    def crossover(self,p1,p2):
        mob_c = self.population[0]
        length = mob_c.path_length
        midpoint = randint(1, length)
        for i in range(length):
            if i < midpoint:
                mob_c.path[i]=p1.path[i]
            else:
                mob_c.path[i] = p2.path[i]

        return mob_c
    """
class FTarget(pg.sprite.Sprite):
    def __init__(self):
        #self.groups = target_sprite
        pg.sprite.Sprite.__init__(self)
        self.image = pg.Surface((30,30))
        self.image.fill(RED)
        self.rect = self.image.get_rect()
        self.pos = vec(1100,HEIGHT/2)
        self.rect.center = self.pos


class Mob(pg.sprite.Sprite):
    def __init__(self,targets,f_target):
        #self.groups = all_sprites
        pg.sprite.Sprite.__init__(self)
        self.image = pg.Surface((MOB_SIZE, MOB_SIZE))
        self.image.fill(YELLOW)
        self.rect = self.image.get_rect()
        #self.pos = vec(randint(0, WIDTH), randint(0, HEIGHT))
        self.pos = vec(400,400)
        self.vel = vec(MAX_SPEED, 0).rotate(uniform(0, 360))
        self.acc = vec(0, 0)
        self.rect.center = self.pos
        self.k=0
        self.path = targets
        self.path_length = len(self.path)
        self.generation = 0
        self.target = f_target
        self.fitness = 0
        self.stopped = 0

    def crossover(self,partner):
        mob = Mob(self.path,self.target)
        length = self.path_length
        midpoint = randint(1, length)
        for i in range(length):
            if i < midpoint:
                mob.path[i] = self.path[i]
            else:
                mob.path[i] = partner.path[i]

        return mob

    def mutate(self):
        for i in range(self.path_length):
            rand = random()
            #print("rand ",rand)
            if rand < mutation_rate:
                #print(self.path[i])
                #self.path[i]*=(random()*0.4+0.8)
                self.path[i]+=vec(uniform(-20,50),uniform(-20,50))
                self.path[i].rotate(uniform(0, 360))
                #print(self.path[i])

    def calc_fitness(self):
        f = self.path[len(self.path)-1].distance_to(self.target.pos)

        if f!=0:
            f = 1 / f * 100
            self.fitness = f
        else:
            self.fitness = 100

    def collided(self):
        self.acc = vec(0, 0)
        self.vel = vec(0, 0)
        self.stopped = 1
        self.fitness = 100
        self.image.fill(RED)

    def seek(self,target):
        self.desired = (target-self.pos).normalize()* randint(1,MAX_SPEED)
        steer = (self.desired-self.vel)
        if steer.length() > MAX_FORCE:
            steer.scale_to_length(MAX_FORCE)
        return steer
    def update(self):
        # self.follow_mouse()
        #self.acc = self.seek_with_approach(pg.mouse.get_pos())
        if self.k<len(self.path):
            self.acc = self.seek(self.path[self.k])
            if self.pos.distance_to(self.path[self.k]) < 20:
                self.k += 1
        else:
            #self.acc = self.seek(pg.mouse.get_pos())
            self.acc = vec(0,0)
            self.vel = vec(0,0)
            self.stopped = 1
        # equations of motion
        self.vel += self.acc
        if self.vel.length() > MAX_SPEED:
            self.vel.scale_to_length(MAX_SPEED)
        self.pos+=self.vel
        #if self.pos.x > WIDTH:
            #self.pos.x = WIDTH
        #if self.pos.x < 0:
            #self.pos.x = 0
        #if self.pos.y > HEIGHT:
            #self.pos.y = HEIGHT
        #if self.pos.y < 0:
            #self.pos.y = 0
        self.rect.center = self.pos



pg.init()
screen = pg.display.set_mode((WIDTH, HEIGHT))
clock = pg.time.Clock()

all_sprites = pg.sprite.Group()
target_sprite = pg.sprite.Group()
f = FTarget()
target_sprite.add(f)
p = Population(f)

for i in range(population_num):
    m=Mob(path_list[i],f)
    all_sprites.add(m)
    p.add_mob(m)
    #print(m.calc_fitness())
p.calc_fitness()

paused = False
running = True
while running:
    clock.tick(FPS)
    for event in pg.event.get():
        if event.type == pg.QUIT:
            running = False
        if event.type == pg.KEYDOWN:
            if event.key == pg.K_ESCAPE:
                running = False
            if event.key == pg.K_SPACE:
                paused = not paused

    if not paused:
        all_sprites.update()

    collided_mobs=pg.sprite.spritecollide(f,all_sprites,False)
    for m in collided_mobs:
        m.collided()


    p.is_stopped()
    if p.stopped==1:
        p.selection()
        p.calc_fitness()
        for mob in all_sprites:
            mob.kill()
        for i in range(population_num):
            #print("dodaj moba ",i)
            all_sprites.add(p.population[i])
            #print(p.population[i].path)
        p.stopped = 0
        print("Generation",p.generation)

    pg.display.set_caption("{:.2f}".format(clock.get_fps()))
    screen.fill(DARKGRAY)
    all_sprites.draw(screen)
    target_sprite.draw(screen)
    pg.display.flip()